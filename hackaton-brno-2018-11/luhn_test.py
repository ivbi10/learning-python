# Luhn test of valid number of bank card

import numpy as np

# dict for changing two digit numbers into one digit numbers
control_dict = {10: 1, 12: 3, 14: 5, 16: 7, 18: 9}

# number of bank card
num = "49927398716"

# reverse the number
rev_num = num[::-1]

# separate numbers by even/odd positions
odd_position = list(rev_num[::2])
even_position = list(rev_num[1::2])

odd_position = np.asarray(odd_position).astype(int)
even_position = np.asarray(even_position).astype(int)

# sum of odd position numbers
s1 = odd_position.sum()

# multiply every even positioned number by 2
s2 = even_position*2

# if two digit, change it into one digit based on control_dict
for k, v in iter(control_dict.items()):
    s2[s2 == k] = v

# sum of transformed even_position numbers
s2 = s2.sum()

print(s1)
print(s2)

if (s1 + s2) % 10 == 0:
    print('Number of bank account is valid.')
