# divisible.py

def getDivisor():
    while True:
        print('Write down divisor number:')
        divisor = int(input())
        if (divisor > 1 and divisor <= 9):
            return divisor
        else:
            print('Warning! 2-9 are accepted as divisor numbers.\n')

divisor = getDivisor()

start = int(input('Write down starting point: '))
stop = int(input('Write down ending point: '))

result = [i for i in range(start, stop+1) if i % divisor == 0]

print('\nThese numbers up to {0} are divided by {1}:\n{2}'.format(stop, divisor, result))

