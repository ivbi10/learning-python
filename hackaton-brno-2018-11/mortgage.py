loan = int(input('How much do you want to borrow?: '))
interest = float(input('At what rate?: '))
years = int(input('How many years?: '))

# transform initial values
months = years * 12
month_interest = interest / 100 / 12

# compute rest of values

monthly_payment = loan*(month_interest * (1 + month_interest)**months) / ((1 + month_interest)**months-1)

total_sum = monthly_payment * months

total_interest = total_sum - loan

for i in range(months+1):
    Interest = total_sum * month_interest
    Principal = monthly_payment - Interest
    Left_to_Pay = total_sum
    print("%d\t%d\t%d\t%d" % (i, Interest, Principal, Left_to_Pay))
    total_sum = Left_to_Pay - (Interest + Principal)
