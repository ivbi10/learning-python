# check prime number
# if True, the number is prime number
# if False, the number is not prime number


def getNum():
    num = 0
    while True:
        print('Enter a number:')
        num = int(input())
        if num >= 2:
            return num
        else:
            print('You need to enter a positive number bigger than 2.\n')


num = getNum()


def gen_primes(N):
    primes = set()
    for n in range(2, N):
        if all(n % p > 0 for p in primes):
            primes.add(n)
            yield n


if num in gen_primes(num+1):
    print('The number is prime number.')
else:
    print('The number is not prime number.')
