l = [1,21,5,3,5,8,321,1,2,2,32,6,9,1,4,6,3,1,2]

result = {}

for i in l:
    result[i] = result.get(i, 0) + 1

sort_results = sorted(result.items(), key=lambda x: x[1], reverse=True)

print("%s\t%s" % ("Item", "Count"))

for key, value in sort_results:
    print("%d\t%d" % (key, value))
