from math import sqrt

l1 = [234, 34]
l2 = [27, 114]

a = abs(l1[0] - l2[0])
b = abs(l1[1] - l2[1])

c = sqrt(a**2 + b**2)

print('Distance between points is {0:.2f}'.format(c))
