# vowels and consonants

s = 'a speech sound that is produced by comparatively open configuration of the vocal tract'

vowels = "aeiou"

count_c = 0
count_v = 0

for i in s:
    if i in vowels:
        count_v += 1
    elif i == ' ':
        continue
    else:
        count_c += 1

print('N of consonants: {0}, N of vowels: {1}'.format(count_c, count_v))



