# game Bulls and Cows
# computer creates four digit number with unique values
# and user has to guess it
# bull means user found correct digit and correct digit position
# cow means user found correct digit but incorrect digit position

import random

# assign a random four digit number to variable num
numbers = random.sample(range(10), 4)
num = ''.join(map(str,numbers))

# user first guess of random number
def getGuess():
        while True:
                print('Guess the four digit number: ')
                guess = input()
                if len(guess) == 4:
                        return guess
                else:
                        print('Warning! You need to enter four digits.\n')   
                        
guess = getGuess()

count = 0

# main loop for the game
while guess != num:
        bull = 0
        cow = 0
        for i in guess:
                if i in num and (num.index(i) == guess.index(i)):
                        bull += 1
                elif i in num and (num.index(i) != guess.index(i)):
                        cow += 1
                else:
                        continue
        print('\nNo of Bulls: {}, No of Cows: {}\n'.format(bull, cow))
        count += 1
        guess = getGuess()

if guess == num:
        print('\nYou made it! Congrats! You needed {} attempts to guess the correct number.'.format(count))
