# find difference between biggest even number
# and biggest odd number in the list

# initial list
nums = [386, 462, 47, 418, 907, 344, 236, 375,
        823, 566, 597, 978, 328, 615, 953, 345,
        399, 162, 758, 219, 918, 237, 412, 566,
        826, 248, 866, 950, 626, 949, 687, 217,
        815, 67, 104, 58, 512, 24, 892, 894, 767,
        553, 81, 379, 843, 831, 445, 742, 717,
        958,743, 527]

# create generators for even and odd numbers
even = (num for num in nums if num % 2 == 0)
odd = (num for num in nums if num % 2 != 0)

# find absolute value of difference
# between max even and max odd number
print(abs(max(even) - max(odd)))
