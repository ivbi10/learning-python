# Caesar cipher

MAX_KEY = 26


def getMessage():
    print('Enter your message: ')
    return input()


message = getMessage()


def getKey():
    key = 0
    while True:
        print('Enter the key number (1-{}): '.format(MAX_KEY))
        key = int(input())
        if (key >= 1 and key <= MAX_KEY):
            return key


key = getKey()


def caesar(s, N):
    translated = ""
    for symbol in s:
        if symbol.isalpha():
            num = ord(symbol)
            num += N

            if symbol.isupper():
                if num > ord('Z'):
                    num -= 26
                elif num < ord('A'):
                    num += 26

            if symbol.islower():
                if num > ord('z'):
                    num -= 26
                elif num < ord('a'):
                    num += 26

            translated += chr(num)
        else:
            translated += symbol

    return translated


print('Your encoded text is: {}'.format(caesar(message, key)))
