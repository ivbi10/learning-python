# remove vowels from a string

message = input('Write your sentence:')
vowels = "aeiou"
new_message = ""

for i in message:
    if i.lower() not in vowels:
        new_message += i

	
print('\nYour sentence without vowels is: {}'.format(new_message))
