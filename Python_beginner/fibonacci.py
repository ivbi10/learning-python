# fibonacci function


def getFib(N, a=0, b=1):
    L = []
    while True:
        (a, b) = (b, a + b)
        if a > N:
            break
        L.append(a)
    return L


print(getFib(100))
