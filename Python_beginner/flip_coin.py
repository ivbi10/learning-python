# Flip a coin - game

# script shows how many times heads and tails
# are tossed by 100 flips of coin

import random

count = 0
head = 0
tail = 0

while count < 100:
    x = random.randint(1,2)
    if x == 1:
        head += 1
    else:
        tail += 1

    count += 1


print('Number of heads: {}. Number of tails: {}'.format(head, tail))
