# Basic information

Exercises were taken from the book **Python Programming for the Absolute Beginner** written by **Michael Dawson**. I am grateful that this book is available online and I could learn from it.
