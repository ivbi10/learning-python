# Guess the number - game

import random

num = random.randint(1, 100)
guess = int(input('Take a guess: '))
attempts = 1

while guess != num:
    if guess > num:
        print('Try lower number.')
    else:
        print('Try higher number.')

    guess = int(input('Take a guess: '))
    attempts += 1

print('You made it! The number is {}. It took you {} tries to guess it. Well done!'.format(num, attempts))

