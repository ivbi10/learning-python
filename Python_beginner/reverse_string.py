# multiple ways how to reverse sting in Python

def reverse_string1(s):
    """
      >>> reverse_string1('message')
      'egassem'
      >>> reverse_string1('Good night!')
      '!thgin dooG'
    """
    new_s = ""
    for i in s:
        new_s = i + new_s
    return new_s

def reverse_string2(s):
    """
      >>> reverse_string2('message')
      'egassem'
      >>> reverse_string2('Good night!')
      '!thgin dooG'
    """
    string = s[::-1]
    return string

def reverse_string3(s):
    """
      >>> reverse_string3('message')
      'egassem'
      >>> reverse_string3('Good night!')
      '!thgin dooG'
    """
    string = "".join(reversed(s))
    return string

if __name__ == '__main__':
    import doctest
    doctest.testmod()
