"""
Task:
- to create a program that will get airports in the United Kingdom
- and display them on standard output using arg parser
"""

# modules needed for the program
import requests
import argparse

# get the json file with airports
url = "https://gist.github.com/tdreyno/4278655/raw/7b0762c09b519f40397e4c3e100b097d861f5588/airports.json"
source = requests.get(url)

if source.status_code != 200:
    raise ApiError('{}'.format(source.status_code))

data = source.json()

# initialise parser with optional arguments
parser = argparse.ArgumentParser(
    description='Script to display information about airports in the United Kingdom.')
parser.add_argument(
    '--cities',
    action='store_true',
    help='display UK cities with an airport')
parser.add_argument(
    '--coords',
    action='store_true',
    help='display coordinates of every airport in the UK')
parser.add_argument(
    '--iata',
    action='store_true',
    help='display IATA code of every airport in the UK')
parser.add_argument(
    '--names',
    action='store_true',
    help='display names of every airport in the UK')
parser.add_argument(
    '--full',
    action='store_true',
    help='display full information about every airport in the UK (city, coordinates, IATA code)')
args = parser.parse_args()

# create list of UK airports that have at least two carriers operating
# from the airport.
GB_airports = list()

for item in data:
    if item['country'] == "United Kingdom" and item['type'] == "Airports" and int(
            item['carriers']) >= 2:
        GB_airports.append(item)

# function to create Python object (dict) with needed data


def get_data():
    """
    Returns a dictionary with relevant data about
    UK airports (name, city, code, coordinates etc.)
    """
    GB_dict = dict()
    for item in GB_airports:
        name = item['name']
        code = item['code']
        city = item['city']
        lat = item['lat']
        lon = item['lon']
        state = item['state']
        value = {
            "code": code,
            "city": city,
            "latitude": float(lat),
            "longitude": float(lon),
            "state": state
        }
        GB_dict[name] = value
    return GB_dict


def get_result():
    """
    Display a standard output in the command line
    based on the task's instructions (airport and its IATA code)
    """
    for k in GB_dict:
        print("{} ({})".format(k, GB_dict[k]["code"]))


def get_cities():
    """
    Function displays cities where airports in the UK are located
    """
    for k in GB_dict:
        print(GB_dict[k]['city'])


def get_coordinates():
    """
    Function displays coordinates of every airport in the UK
    """
    for k in GB_dict:
        lat = GB_dict[k]['latitude']
        lon = GB_dict[k]['longitude']
        print("{}\n[Lat: {}, Lon: {}]\n".format(k, lat, lon))


def get_city_iata():
    """
    Function displays cities where airports in the UK are located
    and IATA codes of respective airports in the city
    """
    for k in GB_dict:
        print("{}, {}".format(GB_dict[k]['city'], GB_dict[k]['code']))


def get_names():
    """
    Function displays names of UK airports in the dictionary
    """
    for name in GB_dict.keys():
        print(name)


def get_all_info():
    """
    Function displays all information from the dictionary
    for every airport (coordinates, code, city, state)
    """
    for k, v in GB_dict.items():
        print("{}\n{}\n".format(k, list(v.values())))


if __name__ == '__main__':
    GB_dict = get_data()

    if args.iata and args.cities:
        get_city_iata()
    elif args.coords:
        get_coordinates()
    elif args.cities:
        get_cities()
    elif args.full:
        get_all_info()
    else:
        get_result()
