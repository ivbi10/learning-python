# At full speed with Python

# Chapter 6 - Dictionaries

# 6.1 Dictionaries

# example dictionary
ages = {
    "Peter": 10,
    "Isabel": 11,
    "Anna": 9,
    "Thomas": 10,
    "Bob": 10,
    "Joseph": 11,
    "Maria": 12,
    "Gabriel": 10,
}

# lenght of the dictionary
len(ages)

# average of values in example dictionary
def find_average(any_dict):
    av = sum(list(any_dict.values())) / len(any_dict)
    return av

# usage of find_average function
find_average(ages)

# finds max value of given dictionary
def find_max_value(any_dict):
    max = 0
    for k, v in any_dict.items():
        if v > max:
            max = v
            name = k
    print("Key: {}, Value: {}".format(name, max))

# usage of find_max_values function
find_max_value(ages)

# update dictionary values with given n number
def new_ages(any_dict, n):
    new_dict = {}
    for k in any_dict.keys():
        any_dict[k] += n
    new_dict.update(any_dict)
    return new_dict

# usage of new_ages function
new_ages(ages, 10)

# Sub-dictionaries

#example dictionary
students = {
"Peter": {"age": 10, "address": "Lisbon"},
"Isabel": {"age": 11, "address": "Sesimbra"},
"Anna": {"age": 9, "address": "Lisbon"},
}

def find_average_age(any_dct):
    num = 0
    for dct in any_dct.values():
        val = dct.get("age")
        num += val
    print("Avegare age is:", num/len(any_dct))

# usage of find_average_age function
find_average_age(students)
