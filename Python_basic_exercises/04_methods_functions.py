# At full speed with Python

# Chapter 4 - Modules and functions

# 4.1 Math module

import math

# greatest common divisor of two numbers
math.gcd(1988, 9765)

# logarhitm with the base 2
math.log2(128)  

# using user input for computation
num = int(input("Write down a random number: "))
math.sin(num)
math.cos(num)
math.tan(num)

# 4.2 Functions

def add2(x,y):
    return x + y

def is_bigger(x,y):
    if x > y:
        return x
    elif x == y:
        print("Both numbers are the same")
    else:
        return y

def is_divisible(a,b):
    if a % b == 0:
        return True
    else:
        return False

def average(list):
    return sum(list)/len(list)

# 4.3 Recursive functions

def factorial(x):
    if x == 0:
        return 1
    else:
        return x * factorial(x-1)

def fib(x):
    a, b = 0, 1
    while b < x:
        print(b, end=", ")
        a, b = b, a+b

# updated fibonacci function with resulting list
def fib2(n):
   results = []
   a, b = 0, 1
   while a < n:
       results.append(a)
       a, b = b, a+b
   return results
