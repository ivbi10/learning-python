# At full speed with Python

# Chapter 3 - Basic datatypes
# 3.1 Exercises with numbers

def average(list):
    """
    Returns average of the list.
    """
    return sum(list)/len(list)

def is_even(x):
    """
    Finds out whether the number is even or odd.
    """
    if x % 2 == 0:
        print("The number is even.")
    else:
        print("The number is odd.")

# 3.2 Exercises with strings

s = "abc"

# prints lenght of a string
len(s)

# string manipulation through indexing
s[0]*3 + s[1]*3 + s[2]*3

s = "aaabbbccc"

# common string methods
s.find("b")
s.replace("a", "X")
s.replace("a", "X", 1)

s = "aaa bbb ccc"

# capitalise letters in string
s.upper()

# string manipulation through indexing and methods
s[0:3].upper() + s[3:7] + s[7:].upper()

# 3.3 Exercises with lists

l = [1,4,9,10,23]

# list slicing
l[1:3]
l[3:]

# appends number to the list
l.append(90)

# using average function defined above
average(l)

# removes sublist from the original list
l[1:3] = []   
