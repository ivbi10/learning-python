# At full speed with Python

# Chapter 5 - Iteration and loops

#5.1 For loops

def find_max(list):
    n = 0
    for value in list:
        if value > n:
            n = value
    print(n)

def find_max_index(list):
    n = 0
    for i, value in enumerate(list):
        if value > n:
            n = value
            index = i
    print("Index:", index, "Value:", n)

def is_sorted(x):
    """
    Finds out whether the list has sorted values in it.
    """
    n = 0
    for value in x:
        if value > n:
            n = value
        else:
            return False
    return True

def has_duplicates(my_list):
    """
    Finds out whether given list has duplicate values in it. Returns True, if it does.
    """
    unique = set(my_list)
    for value in unique:
        count = my_list.count(value)
        if count > 1:
            return True
    return False

def list_duplicates(my_list):
    """
    Prints out all duplicated values in list.
    """
    seen_more = set([x for x in my_list if my_list.count(x) > 1])
    print(list(seen_more))

# 5.2 While loop

def even_odd(n):
    """
    Prints out whether a number is even or odd.
    """
    while n > 0:
        if n % 2 == 0:
            print("Even number:", n)
        else:
            print("Odd number:", n)
        n = n - 1
