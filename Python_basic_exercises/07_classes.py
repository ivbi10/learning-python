# At full speed with Python

# Chapter 7 - Classes

# Classes
class Rectangle:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def width(self):
        return self.x

    def height(self):
        return self.y

    def area(self):
        return self.width() * self.height()

    def circunference(self):
        return (2*self.width()) * (2*self.height())

    def __str__(self):
        return "Coordiantes of the rectangle: width {}, height {}.".format(self.x, self.y)

# Inheritance

class Square (Rectangle):
    def __init__(self, x):
        super().__init__(x, y=None)

    def area(self):
        return self.x ** 2
